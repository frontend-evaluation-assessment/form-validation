

var currentTab = 0; 
const email=document.getElementById('email');
var pattern=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
showTab(currentTab); 

function showTab(n) {
  

  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
    
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  
  fixStepIndicator(n)
}

function nextPrev(n) {
  // tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab:
  currentTab = currentTab + n;
  //reached the end of the form...
  if (currentTab >= x.length) {
    //the form gets submitted:
    document.getElementById("nextBtn").setAttribute("type","submit");
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  //validation of the form fields
  const emailvalue=email.value.trim();
  var x, y, i, valid = true;
  
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    
    if (y[i].value == "") {
      y[i].className += " invalid";
      valid = false;
    }
    
  //for email validate
   if(emailvalue.match(pattern)){
      valid=true;
  }
   else{
    email.className += " invalid";
      valid=false;
  }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; 
}

function fixStepIndicator(n) {
  // removes the "active" class 
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //and adds the "active" class 
  x[n].className += " active";
}

 //For JSON data

   let jsonData =[];

   const addData=(ev)=>{
    ev.preventDefault();
    let data={
        id: Date.now(),
        title:document.getElementById('name').value,
        email:document.getElementById('email').value,
        phone:document.getElementById('phone').value,
        company:document.getElementById('company').value,
        companyType:document.getElementById('list').value,
        message:document.getElementById('message').value
    }
    jsonData.push(data);
    document.querySelector('form').reset();

    console.warn('added',{jsonData});

    localStorage.setItem('jsonData',JSON.stringify(jsonData));
   } 
   
    document.addEventListener('DOMContentLoaded',()=>{
        
        document.getElementById('regForm').addEventListener('submit',addData)
    });

